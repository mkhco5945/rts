import numpy as np


class Query:

    def __init__(self, id, start_point, end_point, threshold):
        self.id = id
        self.start_points = start_point
        self.end_points = end_point
        self.threshold = threshold
        self.lambda_number = 0
        self.sigma_number = 0
        self.poosheshed = 0
        self.signalled = 0

    def value_fits(self, value):
        if self.start_points[0] <= value <= self.end_points[0] + 1:
            return True
        else:
            return False

    def update_signs(self, counter):

        print("Old Query signs:")
        print("Query.id:'", self.id, "' Query.threshold:'", self.threshold, "' Query.poosheshed:'", self.poosheshed,
              "' Query.signalled:'", self.signalled, "' Query.lambda_number:'", self.lambda_number,
              "' Query.sigma_number:'",
              self.sigma_number, "'")

        self.lambda_number = int(self.threshold) / (2 * self.poosheshed)
        self.sigma_number = self.lambda_number + counter

        print("New Query signs:")
        print("Query.id:'", self.id, "' Query.threshold:'", self.threshold, "' Query.poosheshed:'", self.poosheshed,
              "' Query.signalled:'", self.signalled, "' Query.lambda_number:'", self.lambda_number,
              "' Query.sigma_number:'",
              self.sigma_number, "'")
        print("+++++++++++++++++++++++++++++")
