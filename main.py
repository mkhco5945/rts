# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from query import Query
from tree import Tree


def query_is_ok(query_list, query):
    for query_list_query in query_list:
        if query_list_query.start_points[0] == query.start_points[0] and query_list_query.end_points[0] == query.end_points[0]:
            print("Query with start point: '", query.start_points[0], "' and end point: '", query.end_points[0],
                  "' already exists")
            return False
        # if query_list_query.start_point <= query.start_point and query_list_query.end_point >= query.end_point:
        #     print(query_list_query.start_point, " - ", query_list_query.end_point)
        #     print(query.start_point, " - ", query.end_point)
        #     print("----")
        #     return False
    return True


def do_rts():
    all_query_list = [Query(0, [1], [11], 17), Query(1, [2], [16], 7), Query(2, [3], [13], 11), Query(3, [4], [9], 5),
                      Query(4, [5], [15], 20)
        , Query(5, [5], [15], 9), Query(6, [6], [8], 5), Query(7, [7], [14], 10), Query(8, [10], [12], 3)]
    # all_query_list = [Query(2, [3], [13], 11)]

    query_list = []

    for query in all_query_list:
        if query_is_ok(query_list, query):
            query_list.append(query)

    tree = Tree(query_list)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    do_rts()
    # l = [1,"ali",3]
    # l[l.index("ali")] = 3
    # print(l)
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
