from heapq import heapify, heappush, heappop
import numpy as np


class Node:

    def __init__(self, id):
        self.id = id
        self.counter = 0
        self.query_list = []
        self.range = []
        self.heap = []
        heapify(self.heap)
        self.right_child = None
        self.left_child = None
        self.min_heap_sigma = 1000

    def calculate_min_heap(self):
        self.heap = []
        # print(self.query_list)
        for query in self.query_list:
            heappush(self.heap, query.sigma_number)
        if len(self.heap) != 0:
            min_heap_sigma = heappop(self.heap)
            self.min_heap_sigma = min_heap_sigma
            heappush(self.heap, min_heap_sigma)
            print("calculated min_heap: ", self.min_heap_sigma)

    def calculate_signs(self):
        for query in self.query_list:
            query.lambda_number = int((query.threshold) / (2 * query.poosheshed))
            query.sigma_number = query.lambda_number + self.counter
            print("first set: \nnode.id:'", self.id, "' query.id:'", query.id, "' query.lambda_number:'",
                  query.lambda_number,
                  "' query.sigma_number:'", query.sigma_number, "'\n****************************")

    def update_min_heap_queries(self):
        if len(self.heap) != 0:
            heappop(self.heap)

    def update_queries(self):
        for query in self.query_list:
            # print(query.threshold, self.counter)
            self.query_list[self.query_list.index(query)].threshold -= self.counter
            query.signalled += 1
            print("Updating queries in node:\nnode.id:'", self.id, "' query.id:'",
                  self.query_list[self.query_list.index(query)].id, "' query.threshold:'",
                  self.query_list[self.query_list.index(query)].threshold, "' query.lambda_number: ",
                  query.lambda_number,
                  "query.sigma_number:'", query.sigma_number, "'\n****************************")
