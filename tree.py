import random
import numpy as np
from node import Node


class Tree:

    def __init__(self, query_list):
        self.query_list = query_list
        self.tree = []  # []
        self.create_tree()
        self.tree.reverse()
        # for node in self.tree:
        #     print(node.range)
        self.allocate_queries_to_nodes(self.query_list.copy())
        self.surject_queries()

        # for q in self.query_list:
        #     print(q.id, q.start_points[0], q.end_points[0], q.threshold)

        for node in self.tree:
            node.calculate_signs()
            node.calculate_min_heap()

        while len(self.query_list) > 0:
            print("Main query_list length: ", len(self.query_list))
            # for i in range(0, len(self.query_list)):
            #     print("query.id: ", self.query_list[i].id, " query.poosheshed: ", self.query_list[i].poosheshed,
            #           "query.treshold: ", self.query_list[i].threshold)

            print("General node info: (1000 is the default min_heap_sigma)")
            for node in self.tree:
                print("node.id:'", node.id, "' node.couter:'", self.tree[self.tree.index(node)].counter,
                      "' node.min_heap_sigma:'", self.tree[self.tree.index(node)].min_heap_sigma,
                      "' node.heap length: ",
                      len(self.tree[self.tree.index(node)].heap))
                print("---------------------------------------------------------")
                if self.tree[self.tree.index(node)].counter >= self.tree[self.tree.index(node)].min_heap_sigma:
                    self.tree[self.tree.index(node)].update_queries()
                    self.tree[self.tree.index(node)].update_min_heap_queries()

            for query in self.query_list:
                # query.signalled += 1
                if query.signalled == query.poosheshed:
                    for node in self.tree:
                        if query in node.query_list:
                            query.update_signs(node.counter)
                    query.signalled = 0

            value = random.randint(1, 16)

            for query in self.query_list:
                if query.threshold <= 0:
                    self.query_list.remove(query)
                    for node in self.tree:
                        if query in node.query_list:
                            node.query_list.remove(query)
                            node.calculate_min_heap()
                    continue
                if query.value_fits(value):
                    for node in self.tree:
                        if query in node.query_list:
                            # print(node.id, self.tree[self.tree.index(node)].counter)
                            self.tree[self.tree.index(node)].counter += 1
                            # print(node.id, self.tree[self.tree.index(node)].counter)

            print("===================================")

    def calculate_max_start_points(self):
        maxi = 0
        for query in self.query_list:
            if maxi <= query.end_points[0]:
                maxi = query.end_points[0]
        return maxi

    def calculate_min_start_points(self):
        mini = 1000000
        for query in self.query_list:
            if mini >= query.start_points[0]:
                mini = query.start_points[0]
        return mini

    def create_tree(self):
        max_end_points = self.calculate_max_start_points()
        min_start_points = self.calculate_min_start_points()
        # max_end_points = 17
        # min_start_points = 1
        branches = int(np.log2(max_end_points - min_start_points + 1))

        node_counter = 0

        for i in range(branches + 1):
            branch_counter = np.power(2, branches - i + 1)
            # print(branch_counter)
            for j in range(np.power(2, branches - i), 0, -1):
                node = Node(node_counter)
                if i == 0:
                    node.range = [j, j + 1]
                    self.tree.append(node)
                    # print(node.range)
                else:
                    self.tree.append(node)
                    node.right_child = self.tree[node_counter - branch_counter]
                    branch_counter -= 1
                    node.left_child = self.tree[node_counter - branch_counter]
                    node.range = [node.left_child.range[0], node.right_child.range[1]]
                    # print(node.range)

                node_counter += 1

    def allocate_queries_to_nodes(self, query_list_copy):
        # query_list_copy = self.query_list.copy()
        for query in query_list_copy.copy():
            stp0 = str(query.start_points[0])
            end0 = str(query.end_points[0])
            st_number = 1
            node_num = 0
            while node_num < len(self.tree):
                i = 0

                while i < st_number:

                    if self.tree[node_num].range[0] == query.start_points[i] and self.tree[node_num].range[1] <= \
                            query.end_points[i]:
                        # print("in if: ", 30 - node_num)
                        # print(self.tree[node_num].range[0], self.tree[node_num].range[1])
                        flag = True
                        for q in self.tree[node_num].query_list:
                            if q.id == query.id:
                                flag = False
                        if flag:
                            self.tree[node_num].query_list.append(query)
                            query_list_copy[query_list_copy.index(query)].start_points[i] = self.tree[node_num].range[1]
                    elif self.tree[node_num].range[0] > query.start_points[i] and self.tree[node_num].range[1] <= \
                            query.end_points[i]:
                        # print("in elif: ", 30-node_num)
                        # print(self.tree[node_num].range[0], self.tree[node_num].range[1])
                        flag = True
                        for q in self.tree[node_num].query_list:
                            if q.id == query.id:
                                flag = False
                        if flag:
                            self.tree[node_num].query_list.append(query)
                            query_list_copy[query_list_copy.index(query)].start_points.append(
                                self.tree[node_num].range[1])
                            query_list_copy[query_list_copy.index(query)].end_points.append(
                                query_list_copy[query_list_copy.index(query)].end_points[0])
                            query_list_copy[query_list_copy.index(query)].end_points[0] = self.tree[node_num].range[0]
                            i = 0
                            st_number += 1
                            node_num = -1
                            break
                    for k in range(len(query_list_copy[query_list_copy.index(query)].start_points)):
                        st = query_list_copy[query_list_copy.index(query)].start_points[i]
                        end = query_list_copy[query_list_copy.index(query)].end_points[i]
                        # print(st, end)
                        if st == end:
                            query_list_copy[query_list_copy.index(query)].start_points.remove(st)
                            query_list_copy[query_list_copy.index(query)].end_points.remove(end)
                            st_number -= 1
                            break
                    i += 1
                node_num += 1

            query_list_copy[query_list_copy.index(query)].end_points.append(int(end0))
            query_list_copy[query_list_copy.index(query)].start_points.append(int(stp0))

    def calculate_surjection_for_queries(self, query):
        poosheshed = 0
        for node in self.tree:
            for node_query in node.query_list:
                if query.id == node_query.id:
                    poosheshed += 1
                    break
        return poosheshed

    def surject_queries(self):
        for i in range(0, len(self.query_list)):
            self.query_list[i].poosheshed = self.calculate_surjection_for_queries(self.query_list[i])
            print("query.id:' ", self.query_list[i].id, "' query.poosheshed:'", self.query_list[i].poosheshed,
                  "' query.treshold:'", self.query_list[i].threshold, "'")
        for node in self.tree:
            print("node id:", node.id)
            for q in node.query_list:
                print("node.query.id: ", q.id, " - ")
            print("------")
